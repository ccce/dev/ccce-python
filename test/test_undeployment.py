"""
    CCCE API

    CCCE backend  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import ccce
from ccce.model.undeployment import Undeployment


class TestUndeployment(unittest.TestCase):
    """Undeployment unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUndeployment(self):
        """Test Undeployment"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Undeployment()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
