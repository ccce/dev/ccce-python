"""
    CCCE API

    CCCE backend  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import ccce
from ccce.model.deployment import Deployment
from ccce.model.ioc_version import IocVersion
globals()['Deployment'] = Deployment
globals()['IocVersion'] = IocVersion
from ccce.model.ioc import Ioc


class TestIoc(unittest.TestCase):
    """Ioc unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testIoc(self):
        """Test Ioc"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Ioc()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
