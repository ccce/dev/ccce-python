# ccce.DeploymentsApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_undeployment**](DeploymentsApi.md#create_undeployment) | **POST** /api/v1/deployments/undeploy/{iocId} | Start undeployment
[**fetch_command**](DeploymentsApi.md#fetch_command) | **GET** /api/v1/deployments/commands/{commandId} | Get ad-hoc command
[**fetch_command_details**](DeploymentsApi.md#fetch_command_details) | **GET** /api/v1/deployments/commands/jobs/{awxCommandId} | Get ad-hoc command details
[**fetch_deployment**](DeploymentsApi.md#fetch_deployment) | **GET** /api/v1/deployments/{deploymentId} | Get deployment
[**fetch_deployment_job_details**](DeploymentsApi.md#fetch_deployment_job_details) | **GET** /api/v1/deployments/jobs/{awxJobId} | Get deployment job details
[**list_commands_by_deployment**](DeploymentsApi.md#list_commands_by_deployment) | **GET** /api/v1/deployments/commands | Get ad-hoc command list for a specific deployment
[**list_deployments**](DeploymentsApi.md#list_deployments) | **GET** /api/v1/deployments | List deployments
[**start_ioc**](DeploymentsApi.md#start_ioc) | **POST** /api/v1/deployments/{iocId}/start | Start an existing IOC
[**stop_ioc**](DeploymentsApi.md#stop_ioc) | **POST** /api/v1/deployments/{iocId}/stop | Stop an existing IOC
[**update_and_deploy_ioc**](DeploymentsApi.md#update_and_deploy_ioc) | **POST** /api/v1/deployments/{iocId} | Update and deploy an existing IOC


# **create_undeployment**
> [Deployment] create_undeployment(ioc_id, undeployment)

Start undeployment

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.undeployment import Undeployment
from ccce.model.deployment import Deployment
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | The id of the IOC to undeploy
    undeployment = Undeployment(
        comment="comment_example",
    ) # Undeployment | 

    # example passing only required values which don't have defaults set
    try:
        # Start undeployment
        api_response = api_instance.create_undeployment(ioc_id, undeployment)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->create_undeployment: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| The id of the IOC to undeploy |
 **undeployment** | [**Undeployment**](Undeployment.md)|  |

### Return type

[**[Deployment]**](Deployment.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Undeployment created |  -  |
**403** | Forbidden: user doesn&#39;t have the necessary permissions in Gitlab |  -  |
**422** | IOC is not deployed |  -  |
**503** | AWX service exception |  -  |
**409** | Concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**404** | Not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_command**
> AdHocCommand fetch_command(command_id)

Get ad-hoc command

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.ad_hoc_command import AdHocCommand
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    command_id = 1 # int | Unique ID of command

    # example passing only required values which don't have defaults set
    try:
        # Get ad-hoc command
        api_response = api_instance.fetch_command(command_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->fetch_command: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **command_id** | **int**| Unique ID of command |

### Return type

[**AdHocCommand**](AdHocCommand.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**404** | Ad-hoc command not found |  -  |
**200** | A command descriptor |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_command_details**
> AwxCommandDetails fetch_command_details(awx_command_id)

Get ad-hoc command details

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.awx_command_details import AwxCommandDetails
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    awx_command_id = 1 # int | Unique ID of AWX command

    # example passing only required values which don't have defaults set
    try:
        # Get ad-hoc command details
        api_response = api_instance.fetch_command_details(awx_command_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->fetch_command_details: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **awx_command_id** | **int**| Unique ID of AWX command |

### Return type

[**AwxCommandDetails**](AwxCommandDetails.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A command with details |  -  |
**503** | AWX service exception |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |
**404** | AWX command not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_deployment**
> Deployment fetch_deployment(deployment_id)

Get deployment

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.deployment import Deployment
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    deployment_id = 1 # int | Unique ID of deployment

    # example passing only required values which don't have defaults set
    try:
        # Get deployment
        api_response = api_instance.fetch_deployment(deployment_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->fetch_deployment: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deployment_id** | **int**| Unique ID of deployment |

### Return type

[**Deployment**](Deployment.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A deployment with details |  -  |
**404** | Deployment not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_deployment_job_details**
> AwxJobDetails fetch_deployment_job_details(awx_job_id)

Get deployment job details

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.awx_job_details import AwxJobDetails
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    awx_job_id = 1 # int | Unique ID of AWX job

    # example passing only required values which don't have defaults set
    try:
        # Get deployment job details
        api_response = api_instance.fetch_deployment_job_details(awx_job_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->fetch_deployment_job_details: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **awx_job_id** | **int**| Unique ID of AWX job |

### Return type

[**AwxJobDetails**](AwxJobDetails.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**404** | Deployment not found |  -  |
**503** | AWX service exception |  -  |
**200** | AWX job details |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_commands_by_deployment**
> PagedCommandResponse list_commands_by_deployment()

Get ad-hoc command list for a specific deployment

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.paged_command_response import PagedCommandResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | Unique ID of IOC (optional)
    deployment_id = 1 # int | Unique ID of deployment (optional)
    page = 1 # int | Page offset (optional)
    limit = 1 # int | Page size (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get ad-hoc command list for a specific deployment
        api_response = api_instance.list_commands_by_deployment(ioc_id=ioc_id, deployment_id=deployment_id, page=page, limit=limit)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->list_commands_by_deployment: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| Unique ID of IOC | [optional]
 **deployment_id** | **int**| Unique ID of deployment | [optional]
 **page** | **int**| Page offset | [optional]
 **limit** | **int**| Page size | [optional]

### Return type

[**PagedCommandResponse**](PagedCommandResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**404** | Deployment not found |  -  |
**503** | AWX service exception |  -  |
**500** | Service exception |  -  |
**200** | A command with details |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_deployments**
> PagedDeploymentResponse list_deployments()

List deployments

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.paged_deployment_response import PagedDeploymentResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | IOC ID (optional)
    host_cs_entry_id = 1 # int | Host CSEntry ID (optional)
    status = "QUEUED" # str | Status (optional)
    user = "user_example" # str | User name (optional)
    query = "query_example" # str | Search text (Search for IOC name, IOC version, Host name, Description, Created by) (optional)
    order_by = "ID" # str | Order by (optional)
    is_asc = True # bool | Order Ascending (optional)
    page = 1 # int | Page offset (optional)
    limit = 1 # int | Page size (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List deployments
        api_response = api_instance.list_deployments(ioc_id=ioc_id, host_cs_entry_id=host_cs_entry_id, status=status, user=user, query=query, order_by=order_by, is_asc=is_asc, page=page, limit=limit)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->list_deployments: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| IOC ID | [optional]
 **host_cs_entry_id** | **int**| Host CSEntry ID | [optional]
 **status** | **str**| Status | [optional]
 **user** | **str**| User name | [optional]
 **query** | **str**| Search text (Search for IOC name, IOC version, Host name, Description, Created by) | [optional]
 **order_by** | **str**| Order by | [optional]
 **is_asc** | **bool**| Order Ascending | [optional]
 **page** | **int**| Page offset | [optional]
 **limit** | **int**| Page size | [optional]

### Return type

[**PagedDeploymentResponse**](PagedDeploymentResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |
**200** | A paged array of deployments |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **start_ioc**
> AdHocCommand start_ioc(ioc_id)

Start an existing IOC

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.ad_hoc_command import AdHocCommand
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | The id of the IOC to start

    # example passing only required values which don't have defaults set
    try:
        # Start an existing IOC
        api_response = api_instance.start_ioc(ioc_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->start_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| The id of the IOC to start |

### Return type

[**AdHocCommand**](AdHocCommand.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**422** | IOC has no active deployment |  -  |
**201** | IOC start initiated |  -  |
**503** | AWX service exception |  -  |
**409** | Concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**403** | Forbidden: User doesn&#39;t have the necessary permissions in Gitlab |  -  |
**404** | IOC not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stop_ioc**
> AdHocCommand stop_ioc(ioc_id)

Stop an existing IOC

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.ad_hoc_command import AdHocCommand
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | The id of the IOC to stop

    # example passing only required values which don't have defaults set
    try:
        # Stop an existing IOC
        api_response = api_instance.stop_ioc(ioc_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->stop_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| The id of the IOC to stop |

### Return type

[**AdHocCommand**](AdHocCommand.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**422** | IOC has no active deployment |  -  |
**503** | AWX service exception |  -  |
**409** | Concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**403** | Forbidden: User doesn&#39;t have the necessary permissions in Gitlab |  -  |
**404** | IOC not found |  -  |
**500** | Service exception |  -  |
**201** | IOC stop initiated |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_and_deploy_ioc**
> Deployment update_and_deploy_ioc(ioc_id, update_and_deploy_ioc)

Update and deploy an existing IOC

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import deployments_api
from ccce.model.general_exception import GeneralException
from ccce.model.update_and_deploy_ioc import UpdateAndDeployIoc
from ccce.model.deployment import Deployment
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = deployments_api.DeploymentsApi(api_client)
    ioc_id = 1 # int | The id of the IOC to deploy
    update_and_deploy_ioc = UpdateAndDeployIoc(
        comment="comment_example",
        source_version="source_version_example",
        host_cs_entry_id=1,
    ) # UpdateAndDeployIoc | 

    # example passing only required values which don't have defaults set
    try:
        # Update and deploy an existing IOC
        api_response = api_instance.update_and_deploy_ioc(ioc_id, update_and_deploy_ioc)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling DeploymentsApi->update_and_deploy_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| The id of the IOC to deploy |
 **update_and_deploy_ioc** | [**UpdateAndDeployIoc**](UpdateAndDeployIoc.md)|  |

### Return type

[**Deployment**](Deployment.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Deployment created |  -  |
**503** | CSEntry service exception |  -  |
**400** | Incomplete request |  -  |
**409** | Concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**403** | Forbidden: User doesn&#39;t have the necessary permissions in Gitlab |  -  |
**404** | IOC not found |  -  |
**422** | CSEntry host not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |
**424** | Metadata file not found, or not processable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

