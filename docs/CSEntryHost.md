# CSEntryHost


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**fqdn** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**scope** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**user** | **str** |  | [optional] 
**interfaces** | [**[CSEntryInterface]**](CSEntryInterface.md) |  | [optional] 
**is_ioc** | **bool** |  | [optional] 
**device_type** | **str** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**ansible_vars** | **{str: ({str: (bool, date, datetime, dict, float, int, list, str, none_type)},)}** |  | [optional] 
**ansible_groups** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


