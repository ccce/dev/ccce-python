# ccce.IOCsApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_ioc**](IOCsApi.md#create_ioc) | **POST** /api/v1/iocs | Create a new IOC
[**delete_ioc**](IOCsApi.md#delete_ioc) | **DELETE** /api/v1/iocs/{iocId} | Deletes a single IOC based on the ID supplied
[**get_ioc**](IOCsApi.md#get_ioc) | **GET** /api/v1/iocs/{iocId} | Find IOC by ID
[**list_iocs**](IOCsApi.md#list_iocs) | **GET** /api/v1/iocs | List IOCs
[**update_ioc**](IOCsApi.md#update_ioc) | **PUT** /api/v1/iocs/{iocId} | Updating an IOC by ID


# **create_ioc**
> Ioc create_ioc(create_ioc)

Create a new IOC

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import iocs_api
from ccce.model.general_exception import GeneralException
from ccce.model.ioc import Ioc
from ccce.model.create_ioc import CreateIoc
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iocs_api.IOCsApi(api_client)
    create_ioc = CreateIoc(
        external_name_id=1,
        source_url="source_url_example",
    ) # CreateIoc | 

    # example passing only required values which don't have defaults set
    try:
        # Create a new IOC
        api_response = api_instance.create_ioc(create_ioc)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling IOCsApi->create_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_ioc** | [**CreateIoc**](CreateIoc.md)|  |

### Return type

[**Ioc**](Ioc.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**409** | IOC already created |  -  |
**503** | CSEntry service exception |  -  |
**400** | Incomplete request |  -  |
**403** | Forbidden: User doesn&#39;t have the necessary permissions in Gitlab |  -  |
**422** | CSEntry host not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |
**201** | IOC created |  -  |
**424** | Metadata file not found, or not processable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_ioc**
> str delete_ioc(ioc_id)

Deletes a single IOC based on the ID supplied

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import iocs_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iocs_api.IOCsApi(api_client)
    ioc_id = 1 # int | Id of IOC to delete

    # example passing only required values which don't have defaults set
    try:
        # Deletes a single IOC based on the ID supplied
        api_response = api_instance.delete_ioc(ioc_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling IOCsApi->delete_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| Id of IOC to delete |

### Return type

**str**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**403** | Forbidden: User doesn&#39;t have the necessary permissions |  -  |
**409** | Concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**204** | IOC deleted |  -  |
**404** | IOC not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ioc**
> IocDetails get_ioc(ioc_id)

Find IOC by ID

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import iocs_api
from ccce.model.ioc_details import IocDetails
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iocs_api.IOCsApi(api_client)
    ioc_id = 1 # int | Unique ID of IOC

    # example passing only required values which don't have defaults set
    try:
        # Find IOC by ID
        api_response = api_instance.get_ioc(ioc_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling IOCsApi->get_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| Unique ID of IOC |

### Return type

[**IocDetails**](IocDetails.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Found IOC |  -  |
**404** | Ioc not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_iocs**
> PagedIocResponse list_iocs()

List IOCs

### Example


```python
import time
import ccce
from ccce.api import iocs_api
from ccce.model.general_exception import GeneralException
from ccce.model.paged_ioc_response import PagedIocResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = iocs_api.IOCsApi(api_client)
    owner = "owner_example" # str | User name (optional)
    query = "query_example" # str | Search text (Search for Custom name, Description, Created by) (optional)
    order_by = "ID" # str | Order by (optional)
    is_asc = True # bool | Order Ascending (optional)
    page = 1 # int | Page offset (optional)
    limit = 1 # int | Page size (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List IOCs
        api_response = api_instance.list_iocs(owner=owner, query=query, order_by=order_by, is_asc=is_asc, page=page, limit=limit)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling IOCsApi->list_iocs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **str**| User name | [optional]
 **query** | **str**| Search text (Search for Custom name, Description, Created by) | [optional]
 **order_by** | **str**| Order by | [optional]
 **is_asc** | **bool**| Order Ascending | [optional]
 **page** | **int**| Page offset | [optional]
 **limit** | **int**| Page size | [optional]

### Return type

[**PagedIocResponse**](PagedIocResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of IOCs |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_ioc**
> Ioc update_ioc(ioc_id, ioc_update_request)

Updating an IOC by ID

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import iocs_api
from ccce.model.general_exception import GeneralException
from ccce.model.ioc_update_request import IOCUpdateRequest
from ccce.model.ioc import Ioc
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = iocs_api.IOCsApi(api_client)
    ioc_id = 1 # int | Unique ID of IOC
    ioc_update_request = IOCUpdateRequest(
        owner="owner_example",
        external_name_id=1,
        source_url="source_url_example",
    ) # IOCUpdateRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Updating an IOC by ID
        api_response = api_instance.update_ioc(ioc_id, ioc_update_request)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling IOCsApi->update_ioc: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_id** | **int**| Unique ID of IOC |
 **ioc_update_request** | [**IOCUpdateRequest**](IOCUpdateRequest.md)|  |

### Return type

[**Ioc**](Ioc.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | IOC updated |  -  |
**403** | Forbidden: User doesn&#39;t have the necessary permissions |  -  |
**409** | IOC already created or concurrent deployment, undeployment, start or stop for IOC is ongoing |  -  |
**404** | Ioc not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

