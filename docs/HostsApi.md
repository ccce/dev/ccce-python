# ccce.HostsApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**find_associated_iocs_by_host_id**](HostsApi.md#find_associated_iocs_by_host_id) | **GET** /api/v1/hosts/{hostCSEntryId}/iocs | Find associated IOCs for the host
[**find_host_by_id**](HostsApi.md#find_host_by_id) | **GET** /api/v1/hosts/{hostCSEntryId} | Find host by CSEntry ID
[**list_hosts**](HostsApi.md#list_hosts) | **GET** /api/v1/hosts | List CSEntry hosts


# **find_associated_iocs_by_host_id**
> AssociatedIocs find_associated_iocs_by_host_id(host_cs_entry_id)

Find associated IOCs for the host

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import hosts_api
from ccce.model.general_exception import GeneralException
from ccce.model.associated_iocs import AssociatedIocs
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hosts_api.HostsApi(api_client)
    host_cs_entry_id = 1 # int | Unique CSEntry ID of the host

    # example passing only required values which don't have defaults set
    try:
        # Find associated IOCs for the host
        api_response = api_instance.find_associated_iocs_by_host_id(host_cs_entry_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling HostsApi->find_associated_iocs_by_host_id: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_cs_entry_id** | **int**| Unique CSEntry ID of the host |

### Return type

[**AssociatedIocs**](AssociatedIocs.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Found Host |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_host_by_id**
> CSEntryHostWithStatus find_host_by_id(host_cs_entry_id)

Find host by CSEntry ID

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import hosts_api
from ccce.model.general_exception import GeneralException
from ccce.model.cs_entry_host_with_status import CSEntryHostWithStatus
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hosts_api.HostsApi(api_client)
    host_cs_entry_id = 1 # int | Unique CSEntry ID of the host

    # example passing only required values which don't have defaults set
    try:
        # Find host by CSEntry ID
        api_response = api_instance.find_host_by_id(host_cs_entry_id)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling HostsApi->find_host_by_id: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_cs_entry_id** | **int**| Unique CSEntry ID of the host |

### Return type

[**CSEntryHostWithStatus**](CSEntryHostWithStatus.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Found Host |  -  |
**404** | Not found |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_hosts**
> PagedCSEntryHostResponse list_hosts()

List CSEntry hosts

### Example


```python
import time
import ccce
from ccce.api import hosts_api
from ccce.model.general_exception import GeneralException
from ccce.model.paged_cs_entry_host_response import PagedCSEntryHostResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = hosts_api.HostsApi(api_client)
    query = "query_example" # str | Search text, this query string is passed directly to Elasticsearch. E.g.: To search all hosts where the string \"archiver\" appears in any field, use \"archiver\".To restrict the search to only the name field, use \"name:archiver\".Note that if you pass \"name:arch\", this will not match archiver as elasticsearch uses keywords for string matching. In this case you’d need to use a wildcard: \"name:arch*\". (optional)
    page = 1 # int | Page offset (optional)
    limit = "limit_example" # str | Page size (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List CSEntry hosts
        api_response = api_instance.list_hosts(query=query, page=page, limit=limit)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling HostsApi->list_hosts: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **str**| Search text, this query string is passed directly to Elasticsearch. E.g.: To search all hosts where the string \&quot;archiver\&quot; appears in any field, use \&quot;archiver\&quot;.To restrict the search to only the name field, use \&quot;name:archiver\&quot;.Note that if you pass \&quot;name:arch\&quot;, this will not match archiver as elasticsearch uses keywords for string matching. In this case you’d need to use a wildcard: \&quot;name:arch*\&quot;. | [optional]
 **page** | **int**| Page offset | [optional]
 **limit** | **str**| Page size | [optional]

### Return type

[**PagedCSEntryHostResponse**](PagedCSEntryHostResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of CSEntry hosts |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

