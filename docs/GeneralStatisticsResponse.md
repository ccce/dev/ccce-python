# GeneralStatisticsResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number_of_active_hosts** | **int** |  | [optional] 
**number_of_created_iocs** | **int** |  | [optional] 
**number_of_active_iocs** | **int** |  | [optional] 
**number_of_deployments** | **int** |  | [optional] 
**number_of_active_deployments** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


