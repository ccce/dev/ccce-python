# ccce.LokiApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetch_proc_serv_log_lines**](LokiApi.md#fetch_proc_serv_log_lines) | **GET** /api/v1/loki/procserv/host/{hostName} | Fetches procServ Log lines for a specific host
[**fetch_syslog_lines**](LokiApi.md#fetch_syslog_lines) | **GET** /api/v1/loki/syslog/host/{hostName} | Fetches syslog lines for a specific host


# **fetch_proc_serv_log_lines**
> LokiResponse fetch_proc_serv_log_lines(host_name, ioc_name)

Fetches procServ Log lines for a specific host

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import loki_api
from ccce.model.loki_response import LokiResponse
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = loki_api.LokiApi(api_client)
    host_name = "hostName_example" # str | Host name (without network part)
    ioc_name = "iocName_example" # str | Name of the IOC to get procServ logs
    time_range = 1 # int | Time range (in minutes) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Fetches procServ Log lines for a specific host
        api_response = api_instance.fetch_proc_serv_log_lines(host_name, ioc_name)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling LokiApi->fetch_proc_serv_log_lines: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Fetches procServ Log lines for a specific host
        api_response = api_instance.fetch_proc_serv_log_lines(host_name, ioc_name, time_range=time_range)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling LokiApi->fetch_proc_serv_log_lines: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_name** | **str**| Host name (without network part) |
 **ioc_name** | **str**| Name of the IOC to get procServ logs |
 **time_range** | **int**| Time range (in minutes) | [optional]

### Return type

[**LokiResponse**](LokiResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Log lines |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_syslog_lines**
> LokiResponse fetch_syslog_lines(host_name)

Fetches syslog lines for a specific host

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import loki_api
from ccce.model.loki_response import LokiResponse
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = loki_api.LokiApi(api_client)
    host_name = "hostName_example" # str | Host name (without network part)
    time_range = 1 # int | Time range (in minutes) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Fetches syslog lines for a specific host
        api_response = api_instance.fetch_syslog_lines(host_name)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling LokiApi->fetch_syslog_lines: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Fetches syslog lines for a specific host
        api_response = api_instance.fetch_syslog_lines(host_name, time_range=time_range)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling LokiApi->fetch_syslog_lines: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host_name** | **str**| Host name (without network part) |
 **time_range** | **int**| Time range (in minutes) | [optional]

### Return type

[**LokiResponse**](LokiResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Log lines |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

