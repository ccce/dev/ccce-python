# CreateIoc

IOC to create

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_name_id** | **int** |  | [optional] 
**source_url** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


