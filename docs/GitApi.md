# ccce.GitApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**info_from_user_name**](GitApi.md#info_from_user_name) | **GET** /api/v1/gitHelper/userInfo/{userName} | User information
[**list_branches**](GitApi.md#list_branches) | **GET** /api/v1/gitHelper/branches | List GitLab projects from the allowed groups
[**list_projects**](GitApi.md#list_projects) | **GET** /api/v1/gitHelper/projects | List Branches
[**list_tags**](GitApi.md#list_tags) | **GET** /api/v1/gitHelper/tags | List Tags
[**list_tags_and_commit_ids**](GitApi.md#list_tags_and_commit_ids) | **GET** /api/v1/gitHelper/tagsAndCommitIds | List Tags and CommitIds
[**user_info**](GitApi.md#user_info) | **GET** /api/v1/gitHelper/userInfo | Current-user information


# **info_from_user_name**
> [UserInfoResponse] info_from_user_name(user_name)

User information

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from ccce.model.user_info_response import UserInfoResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)
    user_name = "userName_example" # str | The username to retrieve info from

    # example passing only required values which don't have defaults set
    try:
        # User information
        api_response = api_instance.info_from_user_name(user_name)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->info_from_user_name: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_name** | **str**| The username to retrieve info from |

### Return type

[**[UserInfoResponse]**](UserInfoResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Information about the current user |  -  |
**503** | Git exception |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_branches**
> [str] list_branches(git_repo)

List GitLab projects from the allowed groups

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)
    git_repo = "gitRepo_example" # str | Git repo URL

    # example passing only required values which don't have defaults set
    try:
        # List GitLab projects from the allowed groups
        api_response = api_instance.list_branches(git_repo)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->list_branches: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **git_repo** | **str**| Git repo URL |

### Return type

**[str]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Git exception |  -  |
**200** | List of Project URLs from the allowed GitLab group |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_projects**
> [str] list_projects()

List Branches

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List Branches
        api_response = api_instance.list_projects()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->list_projects: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**[str]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Git exception |  -  |
**200** | List of Branches for a specific GitLab repo |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_tags**
> [str] list_tags(git_repo)

List Tags

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)
    git_repo = "gitRepo_example" # str | Git repo URL

    # example passing only required values which don't have defaults set
    try:
        # List Tags
        api_response = api_instance.list_tags(git_repo)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->list_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **git_repo** | **str**| Git repo URL |

### Return type

**[str]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | List of Tags for a specific GitLab repo |  -  |
**503** | Git exception |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_tags_and_commit_ids**
> [str] list_tags_and_commit_ids(git_repo)

List Tags and CommitIds

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)
    git_repo = "gitRepo_example" # str | Git repo URL

    # example passing only required values which don't have defaults set
    try:
        # List Tags and CommitIds
        api_response = api_instance.list_tags_and_commit_ids(git_repo)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->list_tags_and_commit_ids: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **git_repo** | **str**| Git repo URL |

### Return type

**[str]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | List of Tags and CommitIds for a specific GitLab repo |  -  |
**503** | Git exception |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **user_info**
> UserInfoResponse user_info()

Current-user information

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import git_api
from ccce.model.general_exception import GeneralException
from ccce.model.user_info_response import UserInfoResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = git_api.GitApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Current-user information
        api_response = api_instance.user_info()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling GitApi->user_info: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**UserInfoResponse**](UserInfoResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Git exception |  -  |
**200** | Information about the current user |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

