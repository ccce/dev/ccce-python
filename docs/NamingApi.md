# ccce.NamingApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetch_iocby_name**](NamingApi.md#fetch_iocby_name) | **GET** /api/v1/naming/IOCNamesByName | Fetches IOC names by name from CCDB


# **fetch_iocby_name**
> [NameResponse] fetch_iocby_name()

Fetches IOC names by name from CCDB

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import naming_api
from ccce.model.general_exception import GeneralException
from ccce.model.name_response import NameResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = naming_api.NamingApi(api_client)
    ioc_name = "iocName_example" # str | IOC name (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Fetches IOC names by name from CCDB
        api_response = api_instance.fetch_iocby_name(ioc_name=ioc_name)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling NamingApi->fetch_iocby_name: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ioc_name** | **str**| IOC name | [optional]

### Return type

[**[NameResponse]**](NameResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | Service exception |  -  |
**503** | Naming service unreachable |  -  |
**401** | Unauthorized |  -  |
**200** | Naming names, and IDs from CCDB |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

