# ccce.AuthenticationApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_user_roles**](AuthenticationApi.md#get_user_roles) | **GET** /api/v1/authentication/roles | Get user roles
[**login**](AuthenticationApi.md#login) | **POST** /api/v1/authentication/login | Login user and acquire JWT token
[**logout**](AuthenticationApi.md#logout) | **DELETE** /api/v1/authentication/logout | Logout
[**token_renew**](AuthenticationApi.md#token_renew) | **POST** /api/v1/authentication/renew | Renewing JWT token


# **get_user_roles**
> [str] get_user_roles()

Get user roles

Get user roles from authorization service

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import authentication_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_api.AuthenticationApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get user roles
        api_response = api_instance.get_user_roles()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling AuthenticationApi->get_user_roles: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**[str]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Login server unavailable |  -  |
**200** | Ok |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **login**
> LoginResponse login(login)

Login user and acquire JWT token

Login user with username, and password in order to acquire JWT token

### Example


```python
import time
import ccce
from ccce.api import authentication_api
from ccce.model.general_exception import GeneralException
from ccce.model.login_response import LoginResponse
from ccce.model.login import Login
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = authentication_api.AuthenticationApi(api_client)
    login = Login(
        user_name="user_name_example",
        password="password_example",
    ) # Login | 

    # example passing only required values which don't have defaults set
    try:
        # Login user and acquire JWT token
        api_response = api_instance.login(login)
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling AuthenticationApi->login: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Login**](Login.md)|  |

### Return type

[**LoginResponse**](LoginResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Login server unavailable |  -  |
**200** | Ok |  -  |
**500** | Service exception |  -  |
**403** | Bad username, and/or password |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **logout**
> {str: (bool, date, datetime, dict, float, int, list, str, none_type)} logout()

Logout

Logging out the user

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import authentication_api
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_api.AuthenticationApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Logout
        api_response = api_instance.logout()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling AuthenticationApi->logout: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**{str: (bool, date, datetime, dict, float, int, list, str, none_type)}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**403** | Logout error |  -  |
**500** | Service exception |  -  |
**200** | Ok |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **token_renew**
> LoginResponse token_renew()

Renewing JWT token

Renewing valid, non-expired JWT token

### Example

* Bearer (JWT) Authentication (bearerAuth):

```python
import time
import ccce
from ccce.api import authentication_api
from ccce.model.general_exception import GeneralException
from ccce.model.login_response import LoginResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearerAuth
configuration = ccce.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ccce.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_api.AuthenticationApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Renewing JWT token
        api_response = api_instance.token_renew()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling AuthenticationApi->token_renew: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**LoginResponse**](LoginResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**503** | Login server unavailable |  -  |
**200** | Ok |  -  |
**500** | Service exception |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

