# ccce.HealthCheckControllerApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_health_check**](HealthCheckControllerApi.md#get_health_check) | **GET** /healthcheck | 


# **get_health_check**
> str get_health_check()



### Example


```python
import time
import ccce
from ccce.api import health_check_controller_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = health_check_controller_api.HealthCheckControllerApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.get_health_check()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling HealthCheckControllerApi->get_health_check: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

