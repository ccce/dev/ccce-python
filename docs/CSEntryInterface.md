# CSEntryInterface


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**network** | **str** |  | [optional] 
**domain** | **str** |  | [optional] 
**ip** | **str** |  | [optional] 
**mac** | **str** |  | [optional] 
**host** | **str** |  | [optional] 
**cnames** | **[str]** |  | [optional] 
**is_main** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


