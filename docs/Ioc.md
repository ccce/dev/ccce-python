# Ioc


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**description** | **str** |  | [optional] 
**owner** | **str** |  | [optional] 
**status** | **str, none_type** |  | [optional] 
**created_by** | **str** |  | [optional] 
**latest_version** | [**IocVersion**](IocVersion.md) |  | [optional] 
**active_deployment** | [**Deployment**](Deployment.md) |  | [optional] 
**has_local_commits** | **bool** |  | [optional] 
**name_changed_in_ccdb** | **bool** |  | [optional] 
**name_changed_in_naming** | **bool** |  | [optional] 
**dirty** | **bool** |  | [optional] 
**service_disabled** | **bool** |  | [optional] 
**cellmode_used** | **bool** |  | [optional] 
**essioc_missing** | **bool** |  | [optional] 
**active** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


