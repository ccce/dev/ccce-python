# Deployment


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**created_by** | **str** |  | [optional] 
**start_date** | **datetime** |  | [optional] 
**end_date** | **datetime** |  | [optional] 
**comment** | **str** |  | [optional] 
**ioc_name** | **str** |  | [optional] 
**version** | [**IocVersion**](IocVersion.md) |  | [optional] 
**host** | [**Host**](Host.md) |  | [optional] 
**status** | **str** |  | [optional] 
**awx_job_id** | **int** |  | [optional] 
**undeployment** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


