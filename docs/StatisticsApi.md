# ccce.StatisticsApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**active_ioc_history**](StatisticsApi.md#active_ioc_history) | **GET** /api/v1/statistics/activeIocHistory | Active IOC history from Prometheus for statistics
[**currently_active_iocs**](StatisticsApi.md#currently_active_iocs) | **GET** /api/v1/statistics/activeIocStatistics | Currently active IOC statistics
[**deployment_statistics**](StatisticsApi.md#deployment_statistics) | **GET** /api/v1/statistics/deploymentStatistics | Deployment statistics
[**general_statistics**](StatisticsApi.md#general_statistics) | **GET** /api/v1/statistics/generalStatistics | Set of statistics
[**ioc_deployment_history**](StatisticsApi.md#ioc_deployment_history) | **GET** /api/v1/statistics/deployedIocHistory | Deployed IOC history from DB for statistics
[**ioc_statistics**](StatisticsApi.md#ioc_statistics) | **GET** /api/v1/statistics/iocStatistics | IOC statistics


# **active_ioc_history**
> [ActiveIOCSForHistoryResponse] active_ioc_history()

Active IOC history from Prometheus for statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.active_iocs_for_history_response import ActiveIOCSForHistoryResponse
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Active IOC history from Prometheus for statistics
        api_response = api_instance.active_ioc_history()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->active_ioc_history: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[ActiveIOCSForHistoryResponse]**](ActiveIOCSForHistoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | History about active IOCs for statistics |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **currently_active_iocs**
> [ActiveIocStatisticsResponse] currently_active_iocs()

Currently active IOC statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.general_exception import GeneralException
from ccce.model.active_ioc_statistics_response import ActiveIocStatisticsResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Currently active IOC statistics
        api_response = api_instance.currently_active_iocs()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->currently_active_iocs: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[ActiveIocStatisticsResponse]**](ActiveIocStatisticsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |
**200** | Statistics about currently active IOCs |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deployment_statistics**
> [DeploymentStatisticsResponse] deployment_statistics()

Deployment statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.general_exception import GeneralException
from ccce.model.deployment_statistics_response import DeploymentStatisticsResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Deployment statistics
        api_response = api_instance.deployment_statistics()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->deployment_statistics: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[DeploymentStatisticsResponse]**](DeploymentStatisticsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Statistics about IOCs, deployments, and hosts |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **general_statistics**
> GeneralStatisticsResponse general_statistics()

Set of statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.general_exception import GeneralException
from ccce.model.general_statistics_response import GeneralStatisticsResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Set of statistics
        api_response = api_instance.general_statistics()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->general_statistics: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**GeneralStatisticsResponse**](GeneralStatisticsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Statistics about IOCs, deployments, and hosts |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ioc_deployment_history**
> [ActiveIOCSForHistoryResponse] ioc_deployment_history()

Deployed IOC history from DB for statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.active_iocs_for_history_response import ActiveIOCSForHistoryResponse
from ccce.model.general_exception import GeneralException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Deployed IOC history from DB for statistics
        api_response = api_instance.ioc_deployment_history()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->ioc_deployment_history: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[ActiveIOCSForHistoryResponse]**](ActiveIOCSForHistoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | History about deployed IOCs from DB for statistics |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ioc_statistics**
> [DeploymentOnHost] ioc_statistics()

IOC statistics

### Example


```python
import time
import ccce
from ccce.api import statistics_api
from ccce.model.general_exception import GeneralException
from ccce.model.deployment_on_host import DeploymentOnHost
from pprint import pprint
# Defining the host is optional and defaults to http://localhost:8080
# See configuration.py for a list of all supported configuration parameters.
configuration = ccce.Configuration(
    host = "http://localhost:8080"
)


# Enter a context with an instance of the API client
with ccce.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = statistics_api.StatisticsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # IOC statistics
        api_response = api_instance.ioc_statistics()
        pprint(api_response)
    except ccce.ApiException as e:
        print("Exception when calling StatisticsApi->ioc_statistics: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[DeploymentOnHost]**](DeploymentOnHost.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Statistics about IOCs deployed to hosts |  -  |
**500** | Service exception |  -  |
**401** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

