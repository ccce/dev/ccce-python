"""
    CCCE API

    CCCE backend  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from ccce.api_client import ApiClient, Endpoint as _Endpoint
from ccce.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from ccce.model.general_exception import GeneralException
from ccce.model.loki_response import LokiResponse


class LokiApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client
        self.fetch_proc_serv_log_lines_endpoint = _Endpoint(
            settings={
                'response_type': (LokiResponse,),
                'auth': [
                    'bearerAuth'
                ],
                'endpoint_path': '/api/v1/loki/procserv/host/{hostName}',
                'operation_id': 'fetch_proc_serv_log_lines',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'host_name',
                    'ioc_name',
                    'time_range',
                ],
                'required': [
                    'host_name',
                    'ioc_name',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'host_name':
                        (str,),
                    'ioc_name':
                        (str,),
                    'time_range':
                        (int,),
                },
                'attribute_map': {
                    'host_name': 'hostName',
                    'ioc_name': 'iocName',
                    'time_range': 'timeRange',
                },
                'location_map': {
                    'host_name': 'path',
                    'ioc_name': 'query',
                    'time_range': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.fetch_syslog_lines_endpoint = _Endpoint(
            settings={
                'response_type': (LokiResponse,),
                'auth': [
                    'bearerAuth'
                ],
                'endpoint_path': '/api/v1/loki/syslog/host/{hostName}',
                'operation_id': 'fetch_syslog_lines',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'host_name',
                    'time_range',
                ],
                'required': [
                    'host_name',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'host_name':
                        (str,),
                    'time_range':
                        (int,),
                },
                'attribute_map': {
                    'host_name': 'hostName',
                    'time_range': 'timeRange',
                },
                'location_map': {
                    'host_name': 'path',
                    'time_range': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client
        )

    def fetch_proc_serv_log_lines(
        self,
        host_name,
        ioc_name,
        **kwargs
    ):
        """Fetches procServ Log lines for a specific host  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.fetch_proc_serv_log_lines(host_name, ioc_name, async_req=True)
        >>> result = thread.get()

        Args:
            host_name (str): Host name (without network part)
            ioc_name (str): Name of the IOC to get procServ logs

        Keyword Args:
            time_range (int): Time range (in minutes). [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            LokiResponse
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['host_name'] = \
            host_name
        kwargs['ioc_name'] = \
            ioc_name
        return self.fetch_proc_serv_log_lines_endpoint.call_with_http_info(**kwargs)

    def fetch_syslog_lines(
        self,
        host_name,
        **kwargs
    ):
        """Fetches syslog lines for a specific host  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.fetch_syslog_lines(host_name, async_req=True)
        >>> result = thread.get()

        Args:
            host_name (str): Host name (without network part)

        Keyword Args:
            time_range (int): Time range (in minutes). [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            LokiResponse
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['host_name'] = \
            host_name
        return self.fetch_syslog_lines_endpoint.call_with_http_info(**kwargs)

