
# flake8: noqa

# Import all APIs into this package.
# If you have many APIs here with many many models used in each API this may
# raise a `RecursionError`.
# In order to avoid this, import only the API that you directly need like:
#
#   from .api.awx_api import AWXApi
#
# or import this package, but before doing it, use:
#
#   import sys
#   sys.setrecursionlimit(n)

# Import APIs into API package:
from ccce.api.awx_api import AWXApi
from ccce.api.authentication_api import AuthenticationApi
from ccce.api.deployments_api import DeploymentsApi
from ccce.api.git_api import GitApi
from ccce.api.hosts_api import HostsApi
from ccce.api.iocs_api import IOCsApi
from ccce.api.loki_api import LokiApi
from ccce.api.naming_api import NamingApi
from ccce.api.statistics_api import StatisticsApi
from ccce.api.health_check_controller_api import HealthCheckControllerApi
